jACT-R Eclipse
==============

[![pipeline status](https://gitlab.com/de.monochromata/jactr-eclipse/badges/master/pipeline.svg)](https://gitlab.com/de.monochromata/jactr-eclipse/commits/master)

Eclipse integration of [jACT-R](http://jact-r.org).

This project provides Eclipse plug-ins and an Eclipse product that can be used to
develop, debug and run jACT-R models and to analyze their results. The Eclipse integration
uses [CommonReality](http://gitlab.com/de.monochromata/commonreality) and
[jACT-R](http://gitlab.com/de.monochromata/jactr).

The Maven site is located at http://monochromata.de/maven/sites/org.jactr.eclipse/ .

The ready-to-use executable can be downloaded from http://monochromata.de/eclipse/products/org.jactr.eclipse/).
The Eclipse plug-ins to add jACT-R to an existing Eclipse installation are available from the
[p2 (Eclipse) update site](http://monochromata.de/eclipse/sites/org.jactr.eclipse/).